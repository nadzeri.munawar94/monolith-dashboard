const router = require("express").Router();
const { auth } = require("../../controllers");

const layoutName = (req, res, next) => {
  res.locals.layout = `layouts/authentication`;
  next();
};

router.use(layoutName);
router.get("/login", auth.login);
router.post("/login", auth.api.login);

module.exports = router;
